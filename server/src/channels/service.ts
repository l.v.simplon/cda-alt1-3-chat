import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export const create = async (name: string) => {
    await prisma.channel.create({
        data: {
            name
        }
    })
}

export const getAll = async () => {
    return prisma.channel.findMany();
}
